---
title: "Netflix Party - Scooby-Doo"
timezone: australia/melbourne
start: 2020-10-31T17:00:00
end: 2020-10-31T18:26:00
eventGroups: [vicpah, netflix]
movieId: 115688
synopsisText: "The Mystery Inc. gang have gone their separate ways and have been apart for two years, until they each receive an invitation to Spooky Island. Not knowing that the others have also been invited, they show up and discover an amusement park that affects young visitors in very strange ways."
partyLink: https://www.tele.pe/netflix/39fec230567ae0a4?s=s165
---
VicPAH [Netflix party](https://www.netflixparty.com/) events allow us to share
movies from our own kennels and couches. Join the chat, or just watch along
with the rest of the PAH; it's up to you! All our movies will be synced up so
that they play and pause at the same time so we see the same thing.

Our first movie for the day will be Scooby-Doo

## Party link
[Click here to join](https://www.tele.pe/netflix/2668dccf8c5a6d94?s=s130)

Remember to click the Netflix Party extension icon to join the party!

## Synopsis
The Mystery Inc. gang have gone their separate ways and have been apart for two years, until they each receive an invitation to Spooky Island. Not knowing that the others have also been invited, they show up and discover an amusement park that affects young visitors in very strange ways.
