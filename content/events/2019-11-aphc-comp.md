---
title: APHC main competition
timezone: australia/adelaide
start: 2019-11-15T18:30:00
end: 2019-11-15T23:00:00
eventGroups: [aphc, interstate, major]
link: https://www.geardadelaide.com/events/australian-pup-amp-handler-competition
adr:
  name: The Wakefield Hotel
  street_address: 76 Wakefield Street
  suburb: Adelaide
  state: South Australia
---
The final night of competition for the Australian Pup & Handler Competition.
Come and see who will be Australia’s top puppy and handler!
