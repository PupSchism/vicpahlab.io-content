# Munches
Munches are fairly informal social events where we come and eat food together
to get to know like-minded pups and handlers.

We welcome all members of our diverse community to attend regardless of gender
or sexual expression.

## Dress code
Varies depending on venue, but usually munches are casual events without gear.

Use your best judgement, and wear appropriate cloting for the venue unless a
specific dress code is given.

## Cost
Free to attend, food extra
