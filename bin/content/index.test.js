const fs = require('fs').promises
const fse = require('fs-extra')
const os = require('os')
const path = require('path')
const walk = require('walkdir')

const { ContentPage, ContentRegistry } = require('./index')


function tmpPath() {
  return path.join(
    os.tmpdir(),
    '' + Math.floor(Math.random() * 1000000),
  )
}
async function readFile(filePath) {
  const handle = await fs.open(filePath)
  const content = await handle.readFile()
  try {
    await handle.close()
  } catch(err) {
    console.error(err)
  }
  return content.toString('utf8')
}


describe('ContentPage', () => {
  describe('parse()', () => {
    for (const { rootPath, thisPath, slug, ...exp } of [
      {
        rootPath: '/basepath',
        thisPath: '/basepath/the-slug.md',
        slug: 'the-slug',
        fullSlug: 'the-slug',
        assetsPath: null,
        contentFile: 'the-slug.md',
        isRoot: false,
        listData: { slug: 'the-slug' },
      },
      {
        rootPath: '/basepath/more',
        thisPath: '/basepath/more/the-slug.md',
        slug: 'the-slug',
        fullSlug: 'the-slug',
        assetsPath: null,
        contentFile: 'the-slug.md',
        isRoot: false,
        listData: { slug: 'the-slug' },
      },
      {
        rootPath: '/basepath',
        thisPath: '/basepath/more/the-slug.md',
        slug: 'the-slug',
        fullSlug: 'more/the-slug',
        assetsPath: null,
        contentFile: 'the-slug.md',
        isRoot: false,
        listData: { slug: 'the-slug' },
      },
      {
        rootPath: '/basepath',
        thisPath: '/basepath/more/the-slug/index.md',
        slug: 'the-slug',
        fullSlug: 'more/the-slug',
        assetsPath: 'more/the-slug',
        contentFile: 'index.md',
        isRoot: false,
        listData: { slug: 'the-slug' },
      },
      {
        rootPath: '/basepath',
        thisPath: '/basepath/more/the-slug/_index.md',
        slug: 'the-slug',
        fullSlug: 'more/the-slug',
        assetsPath: 'more/the-slug',
        contentFile: '_index.md',
        isRoot: false,
        listData: { slug: 'the-slug' },
      },
      {
        rootPath: '/basepath/more',
        thisPath: '/basepath/more/the-slug/index.md',
        slug: 'the-slug',
        fullSlug: 'the-slug',
        assetsPath: 'the-slug',
        contentFile: 'index.md',
        isRoot: false,
        listData: { slug: 'the-slug' },
      },
      {
        rootPath: '/basepath/more',
        thisPath: '/basepath/more/the-slug/_index.md',
        slug: 'the-slug',
        fullSlug: 'the-slug',
        assetsPath: 'the-slug',
        contentFile: '_index.md',
        isRoot: false,
        listData: { slug: 'the-slug' },
      },
      {
        rootPath: '/basepath',
        thisPath: '/basepath/index.md',
        slug: 'index',
        fullSlug: 'index',
        assetsPath: null,
        contentFile: 'index.md',
        isRoot: true,
        listData: null,
      },
    ]) {
      test(`${thisPath} in ${rootPath} is ${slug}`, async () => {
        const obj = new ContentPage({ rootPath, thisPath })
        expect(await obj.parse()).toEqual({ slug, ...exp })
      })
    }
  })
  describe('create()', () => {
    const testRootPath = tmpPath()
    const inPath = path.join(testRootPath, 'in')
    const outPath = path.join(testRootPath, 'out')
    beforeEach(() => {
      fse.emptyDirSync(inPath)
      fse.emptyDirSync(outPath)
    })
    afterAll(() => {
      fse.removeSync(testRootPath)
    })

    for (const [inRel, outRel, assets] of [
      ['the-slug.md', 'the-slug/index.md', {}],
      ['the-slug/index.md', 'the-slug/index.md', {}],
      ['the-slug/_index.md', 'the-slug/index.md', {}],
      ['the-slug/more/_index.md', 'the-slug/more/index.md', {}],
      ['the-slug/more/_index.md', 'the-slug/more/index.md', {'mypic.jpg': 'testcontent'}],
      ['index.md', 'index.md', {}],
    ]) {
      test(`file at ${inRel} copies to ${outRel}`, async () => {
        const content = 'this is a test'
        const assetsRel = path.dirname(inRel)
        const thisPath = path.join(inPath, inRel)

        // Write index
        await fse.outputFile(thisPath, content)

        // Write assets
        for (const [fn, asset] of Object.entries(assets)) {
          await fse.outputFile(path.join(inPath, assetsRel, fn), asset)
        }

        // Do test ops
        const page = new ContentPage({
          rootPath: inPath,
          thisPath,
        })
        await page.create(outPath)

        // Check copies index
        expect(await readFile(path.join(outPath, outRel))).toEqual(content)

        // Check copies assets
        for (const [fn, asset] of Object.entries(assets)) {
          expect(await readFile(path.join(outPath, assetsRel, fn))).toEqual(asset)
        }
      })
    }
  })
})


describe('ContentRegistry', () => {
  describe('addPage()', () => {
    for (const suffix of ['slug.md', 'slug/index.md', 'slug/_index.md']) {
      test(`creates registries to firstlevel/slug/index.md with firstlevel/${suffix}`, async () => {
        const rootPath = '/test/path'
        const reg = new ContentRegistry({ rootPath, thisPath: rootPath })
        const page = new ContentPage({ rootPath, thisPath: `${rootPath}/firstlevel/${suffix}` })

        await reg.addPage(page)

        expect(reg.registries).toEqual({
          firstlevel: expect.any(ContentRegistry),
        })
        expect(reg.pages).toEqual({})
        expect(reg.registries.firstlevel.registries).toEqual({})
        expect(reg.registries.firstlevel.pages).toEqual({
          slug: page
        })
      })
    }
    test('handles root index', async () => {
      const rootPath = '/test/path'
      const reg = new ContentRegistry({ rootPath, thisPath: rootPath })
      const page = new ContentPage({ rootPath, thisPath: `${rootPath}/index.md` })

      await reg.addPage(page)

      expect(reg.registries).toEqual({
      })
      expect(reg.pages).toEqual({
        index: page,
      })
    })
    test('handles multiple pages in same registry', async () => {
      const rootPath = '/test/path'
      const reg = new ContentRegistry({ rootPath, thisPath: rootPath })
      const pageFirst = new ContentPage({ rootPath, thisPath: `${rootPath}/firstlevel/first.md` })
      const pageSecond = new ContentPage({ rootPath, thisPath: `${rootPath}/firstlevel/second.md` })

      await reg.addPage(pageFirst)
      await reg.addPage(pageSecond)

      expect(reg.registries).toEqual({
        firstlevel: expect.any(ContentRegistry),
      })
      expect(reg.pages).toEqual({})
      expect(reg.registries.firstlevel.registries).toEqual({})
      expect(reg.registries.firstlevel.pages).toEqual({
        first: pageFirst,
        second: pageSecond,
      })
    })
    test('handles multiple registries', async () => {
      const rootPath = '/test/path'
      const reg = new ContentRegistry({ rootPath, thisPath: rootPath })
      const pageFirst = new ContentPage({ rootPath, thisPath: `${rootPath}/firstlevel/first.md` })
      const pageSecond = new ContentPage({ rootPath, thisPath: `${rootPath}/secondlevel/second.md` })

      await reg.addPage(pageFirst)
      await reg.addPage(pageSecond)

      expect(reg.registries).toEqual({
        firstlevel: expect.any(ContentRegistry),
        secondlevel: expect.any(ContentRegistry),
      })
      expect(reg.pages).toEqual({})
      expect(reg.registries.firstlevel.registries).toEqual({})
      expect(reg.registries.firstlevel.pages).toEqual({
        first: pageFirst,
      })
      expect(reg.registries.secondlevel.registries).toEqual({})
      expect(reg.registries.secondlevel.pages).toEqual({
        second: pageSecond,
      })
    })
    test('handles arbitrary depth', async () => {
      const rootPath = '/test/path'
      const reg = new ContentRegistry({ rootPath, thisPath: rootPath })
      const page = new ContentPage({
        rootPath,
        thisPath: `${rootPath}/firstlevel/secondlevel/thirdlevel/slug.md`,
      })

      await reg.addPage(page)

      let thisReg = reg
      expect(thisReg.registries).toEqual({
        firstlevel: expect.any(ContentRegistry),
      })
      expect(thisReg.pages).toEqual({
      })

      thisReg = thisReg.registries.firstlevel
      expect(thisReg.registries).toEqual({
        secondlevel: expect.any(ContentRegistry),
      })
      expect(thisReg.pages).toEqual({
      })

      thisReg = thisReg.registries.secondlevel
      expect(thisReg.registries).toEqual({
        thirdlevel: expect.any(ContentRegistry),
      })
      expect(thisReg.pages).toEqual({
      })

      thisReg = thisReg.registries.thirdlevel
      expect(thisReg.registries).toEqual({
      })
      expect(thisReg.pages).toEqual({
        slug: page
      })
    })
  })


  describe('filesystem functions', () => {
    const testRootPath = tmpPath()
    const inPath = path.join(testRootPath, 'in')
    const outPath = path.join(testRootPath, 'out')
    beforeEach(() => {
      fse.emptyDirSync(inPath)
      fse.emptyDirSync(outPath)
    })
    afterAll(() => {
      fse.removeSync(testRootPath)
    })

    async function createInFile(relPath, content, assets) {
      const assetsRel = path.dirname(relPath)
      const thisPath = path.join(inPath, relPath)

      // Write index
      await fse.outputFile(thisPath, content)

      // Write assets
      for (const [fn, asset] of Object.entries(assets)) {
        await fse.outputFile(path.join(inPath, assetsRel, fn), asset)
      }
    }

    describe('walk()', () => {
      test('deeply walks a directory, adding content, ignoring assets', async () => {
        const inRel = 'theprefix/theslug/_index.md'
        const content = 'this is a test'
        const assets = {'mypic.jpg': 'testcontent'}

        await createInFile(inRel, content, assets)

        // Do test ops
        const reg = new ContentRegistry({
          rootPath: inPath,
          thisPath: inPath,
        })
        await reg.walk()

        let thisReg = reg
        expect(thisReg.registries).toEqual({
          theprefix: expect.any(ContentRegistry),
        })
        expect(thisReg.pages).toEqual({
        })

        thisReg = thisReg.registries.theprefix
        expect(thisReg.registries).toEqual({
        })
        expect(thisReg.pages).toEqual({
          theslug: expect.any(ContentPage),
        })
        expect(await thisReg.pages.theslug.parse()).toEqual({
          fullSlug: 'theprefix/theslug',
          slug: 'theslug',
          assetsPath: 'theprefix/theslug',
          isRoot: false,
          contentFile: '_index.md',
          listData: { slug: 'theslug' },
        })
      })
      test('handles root index', async () => {
        const inRel = 'index.md'
        const content = 'this is a test'
        await createInFile('index.md', content, {})

        const reg = new ContentRegistry({
          rootPath: inPath,
          thisPath: inPath,
        })
        await reg.walk()
        expect(reg.registries).toEqual({
        })
        expect(reg.pages).toEqual({
          index: expect.any(ContentPage),
        })
      })
    })
    describe('create()', () => {
      test('deeply copies content, assets, indexes', async () => {
        const c = relPath => createInFile(
          relPath,
          `this is ${relPath}`,
          {'mypic.jpg': `this is asset in ${relPath}`}
        )

        await Promise.all([
          c('firstprefix/theslug/_index.md'),
          c('secondprefix/firstslug/_index.md'),
          c('secondprefix/secondslug/_index.md'),
          c('secondprefix/thirdslug/index.md'),
          c('thirdprefix/fourthprefix/fourthslug.md'),
          c('rootslug.md'),
          c('anotherrootslug/index.md'),
          c('index.md'),
        ])

        // Do test ops
        const reg = new ContentRegistry({
          rootPath: inPath,
          thisPath: inPath,
        })
        await reg.walk()
        await reg.create(outPath)

        async function expectSlug(relPath, fullSlug, assets=true) {
          expect(await readFile(`${outPath}/${fullSlug}/index.md`))
            .toEqual(`this is ${relPath}`)
          if (assets) {
            expect(await readFile(`${outPath}/${fullSlug}/mypic.jpg`))
              .toEqual(`this is asset in ${relPath}`)
          }
        }

        expect(await readFile(`${outPath}/index.md`))
          .toEqual(`this is index.md`)
        await expectSlug(
          'firstprefix/theslug/_index.md',
          'firstprefix/theslug',
        )
        await expectSlug(
          'secondprefix/firstslug/_index.md',
          'secondprefix/firstslug',
        )
        await expectSlug(
          'secondprefix/secondslug/_index.md',
          'secondprefix/secondslug',
        )
        await expectSlug(
          'secondprefix/thirdslug/index.md',
          'secondprefix/thirdslug',
        )
        await expectSlug(
          'thirdprefix/fourthprefix/fourthslug.md',
          'thirdprefix/fourthprefix/fourthslug',
          false,
        )
        await expectSlug(
          'rootslug.md',
          'rootslug',
          false,
        )
        await expectSlug(
          'anotherrootslug/index.md',
          'anotherrootslug',
        )

        async function expectIndex(relPath, items) {
          const data = JSON.parse(await readFile(path.join(outPath, relPath, 'index.json')))
          expect(data).toEqual({ items: expect.arrayContaining(items.map(slug => ({ slug }))) })
          expect(data.items.length).toBe(items.length)
        }

        await expectIndex('firstprefix', ['theslug'])
        await expectIndex('secondprefix', ['firstslug', 'secondslug', 'thirdslug'])
        await expectIndex('thirdprefix/fourthprefix', ['fourthslug'])
        await expectIndex('', ['rootslug', 'anotherrootslug'])
      })
    })
  })
})
