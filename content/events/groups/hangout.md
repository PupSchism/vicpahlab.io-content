# VicPAH online hangout
Get online and join your fellow pets and handlers wearing whatever
Pup/Pet/Handler gear you like!

Our meets are open to all diverse gender, and sexual identities whether you're
an experienced Pup or Handler, or simply curious to see if it's something that
may interest you, we have a welcoming and safe space for you to enjoy.

Standard VicPAH rules apply, no nudity, graphic imagery, or hate speech. Please
respect everyone and yourself at all times.
 
You don't need a Google Account to join the meeting. You're also welcome to
join without video/voice, and just use the text chat.
