# COVID-19 and VicPAH

In accordance with the best public health advice available to us, the Victorian government’s roadmap out of lockdown, requirements from many of our venues, and the fact that the VicPAH community has a number of high risk individuals in our community, the committee has come to the conclusion that for the foreseeable future the quickest and only way we are able to host in-person events again is with every attendee being fully vaccinated, or having an approved medical exemption. As such, attendance to VicPAH events will require you to show an official Australian government COVID-19 digital certificate. (Medical exemptions may be accepted based on relevant government advice which is yet to be confirmed)

We are still in the planning stages and are unsure what these events will look like, but as always the safety of every one of our members is our top priority! We are trying to make special efforts to ensure that our members that have been without their community connections for over 18 months now because of their vulnerability are able to come back as soon as they are able.

According to the Victorian roadmap out of lockdown, the earliest that we are likely to have an in-person event is November (note that this is not a guarantee). We would like to remind our members who have not had a second vaccination dose - or their first - that the current waiting period between doses is 6 weeks. For your best chance of being able to attend events as soon as we start having them, we urge you to get both your vaccinations as soon as possible.

We know our community is one of compassion for every one of our members and hope that compassion drives us all to protect every pup, handler, pet, and critter as we start to ease back into seeing each other again.

Stay safe, get vaccinated, pup out.<br/>
*The VicPAH Committee*

## FAQs

These FAQs will be kept up-to-date as new information becomes available.

Details of the Victorian Roadmap and the National Plan can be found here:
- https://www.coronavirus.vic.gov.au/victorias-roadmap
- https://www.australia.gov.au/national-plan

### What exemptions will you accept?
The only case that we are accepting by default is an official Australian COVID-19 digital certificate shown upon entry to any event. Medical exemptions, International vaccine certificates etc. may be accepted based on pending government advice. We will not be granting exemptions based on moral objections or religious grounds.

### What is the process for requesting an exemption?
Email the specifics of your request to bark@vicpah.org.au. We will need to be able to verify any information that you give us so be sure to include any documentation that you’re able to provide, and allow plenty of time for us to follow up. Though we will do our best to provide timely responses, we can’t guarantee how long it could take so the sooner the better.

### I’m a vulnerable member of the community and I’m still concerned about the safety of in-person VicPAH events. What can I do?
Safety is our top concern and we’re eager to hear your thoughts on what we can do to make our events better for everyone! Email bark@vicpah.org.au and we will be happy to discuss your concerns, and hopefully put in place processes that allow you to feel safe when meeting the community again. If we can’t accommodate you, we will still be running our regular online events for anyone not able to attend in person.

### I’m not going to get vaccinated. When will I be allowed to attend again?
We aren’t able to answer at this time. We will still be running our regular online events for anyone not able to attend in person.

### How do I get vaccinated?
The Victorian and Australian governments have a wide range of information and resources available (links provided on the website copy of this Q&A, linked above). If you’re still not sure, you should consult with your GP.

- https://www.coronavirus.vic.gov.au/book-your-vaccine-appointment
- https://www.health.gov.au/initiatives-and-programs/covid-19-vaccines/getting-your-vaccination

### When is the next event going to be? Will there be mats and play space? Will masks be required? And many more questions!
The committee will post news in our usual places (Discord, Facebook, Telegram, and the VicPAH website) as soon as we are able, but at the moment we aren’t able to commit to a date, month, or even format for our next event. You can rest assured though that the committee is working hard to make sure that we are able to get together as soon as we possibly can.
