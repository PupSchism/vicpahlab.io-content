---
title: Midsumma pride march
timezone: australia/melbourne
start: '2022-02-06T09:10:00'
end: '2022-02-06T16:00:00'
link: https://www.midsumma.org.au/info/midsumma-pride-march/
facebook: https://www.facebook.com/midsumma
eventGroups: [vicpah, victoria, midsumma, major, free]
dressCode: |-
  Please wear either puppy gear or something that shows off your puppy pride.
adr:
  street_address: Fitzroy St
  suburb: St Kilda
  state: VIC
---
VicPAH will be marching in the Midsumma Pride March once again this year. We will be marching
alongside dozens of other LGBTQIA+ groups, and we'd love it if you could join us in celebrating
our pride as pups and handlers.

## More informmation
We will be updating when we have more information from the event coordinators
