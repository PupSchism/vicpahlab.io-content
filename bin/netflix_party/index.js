const arg = require('arg');
const axios = require('axios');
const fs = require('fs').promises;
const yaml = require('js-yaml');
const _ = require('lodash');
const moment = require('moment-timezone');
const path = require('path');
const remark = require('remark');
const remarkFrontmatter = require('remark-frontmatter');
const remarkParse = require('remark-parse');
const remarkStringify = require('remark-parse');
const slug = require('slug');
const unified = require('unified');
const vfile = require('to-vfile');

const projPath = path.join(__dirname, '..', '..')
const contentPath = path.join(projPath, 'content', 'events');

const dayOfWeek = 'saturday';

const datestr = 'YYYY-MM-DDTHH:mm:00'

const secrets = require('./secrets.json');


const idxToEng = idx => ({ 0: 'first', 1: 'second', 2: 'final' }[idx]);
const roundUp = (number, group) => Math.ceil(number / group) * group;
function partyLink(url) {
  return url
    ? `[Click here to join](${url})\n\nRemember to click the Netflix Party extension icon to join the party!`
    : 'The party link will be added here shortly before the movie is about to begin.'
}
function titleToMovieName(title) {
  return title.substr(16);
}
const movieText = ({ single, idx, title }) => single
      ? `We will only be watching 1 movie for tonight: ${titleToMovieName(title)}`
      : `Our ${idxToEng(idx)} movie for the day will be ${titleToMovieName(title)}`;
function event(opts) {
  return `---
title: ${JSON.stringify(opts.title)}
timezone: australia/melbourne
start: ${opts.start.format(datestr)}
end: ${opts.end.format(datestr)}
eventGroups: [vicpah, netflix]
movieId: ${opts.movieId}
synopsisText: ${JSON.stringify(opts.synopsisText)}
${opts.partyLink ? 'partyLink: ' + opts.partyLink : ''}
---
VicPAH [Netflix party](https://www.netflixparty.com/) events allow us to share
movies from our own kennels and couches. Join the chat, or just watch along
with the rest of the PAH; it's up to you! All our movies will be synced up so
that they play and pause at the same time so we see the same thing.

${movieText(opts)}

## Party link
${partyLink(opts.partyLink)}

## Synopsis
${opts.synopsisText}
`;
}
const imEmoji = '✉️';
const emojis = [
  '🍿',
  '🎥',
  '🎬',
  '📼',
  '🎥',
  '📺',
  '👩‍💻',
  '👨‍💻',
  '🧑‍💻',
  '🐕',
  '🐶',
  '🎉',
  '🥳',
];


function rawCompiler() {
  function compiler(tree) {
    return tree;
  }
  this.Compiler = compiler;
}


function nextDate(single = false) {
  throw new Error('Next dates arent really working for us');
  const now = moment();
  const next = moment()
        .startOf('month')
        .day(dayOfWeek);
  if (next.month() < now.month()) {
    next.add(4, 'week');
  } else {
    next.add(3, 'week');
  }
  if (single) {
    next.subtract(2, 'week');
    next.set({ hour: 19, minute: 0, second: 0, millisecond: 0 });
  } else {
    next.set({ hour: 17, minute: 0, second: 0, millisecond: 0 });
  }
  return next;
}
function applyStartTime(inDate, single = false) {
  const date = inDate.clone();
  if (single) {
    date.set({ hour: 19, minute: 0, second: 0, millisecond: 0 });
  } else {
    date.set({ hour: 17, minute: 0, second: 0, millisecond: 0 });
  }
  return date;
}
async function listFiles(date, single = false) {
  // const fileRe = single
  //       ? new RegExp(`^${date.format('YYYY-MM')}-[0-9]{2}-netflix-party-.*\.md$`)
  //       : new RegExp(`^${date.format('YYYY-MM')}-netflix-party-.*\.md$`);
  const fileRe = new RegExp(`^${date.format('YYYY-MM-DD')}-netflix-party-.*\.md$`)
  const phRe = new RegExp('-placeholder(-2)?\.md$');
  const files = (await fs.readdir(contentPath)).filter(
    name => fileRe.test(name) && !phRe.test(name)
  ).filter(name => name.indexOf('mean') === -1);
  return files.map(suffix => path.join(contentPath, suffix));
}
async function loadFiles(date, single = false) {
  const processor = unified()
        .use(remarkParse)
        .use(remarkFrontmatter, ['yaml'])
        .use(rawCompiler);
  const files = (await listFiles(date, single))
        .map(filename => vfile.readSync(filename))
        .map(file => {
          const r = processor.processSync(file.contents)
          const fm = r.result.children.find(node => node.type === 'yaml')
          r.history = file.history;
          r.frontmatter = yaml.safeLoad(fm.value);
          r.frontmatter.start = moment
            .tz(r.frontmatter.start, 'utc')
            .tz(r.frontmatter.timezone, true);
          r.frontmatter.end = moment
            .tz(r.frontmatter.end, 'utc')
            .tz(r.frontmatter.timezone, true);
          return r
        })
  files.sort((left, right) => {
    if (left.frontmatter.start.unix() > right.frontmatter.start.unix())
      return 1;
    if (left.frontmatter.start.unix() < right.frontmatter.start.unix())
      return -1;
    return 0
  })
  return files;
}

function sendDiscord(rawMsg, { im = true, cli = true, cliPrefix = true } = {}) {
  const content = rawMsg.trim().split('\n').map(s => s.trim()).join('\n');
  if (cli) {
    if (cliPrefix) console.log(imEmoji, '  DISCORD:');
    console.log(content);
  }
  if (im)
    return axios.post(
      secrets.discord_webhook,
      { content },
    );
}


function processArgs(argv, opts) {
  return _.chain(
    arg(opts, { argv }),
  )
    .mapKeys(
      (v, k) => k.startsWith('--')
        ? _.camelCase(k.substr(2))
        : k
    )
    .value();
}
const dateArg = val => {
  const d = moment(val, 'YYYY-MM-DD', true);
  if (!d.isValid()) throw new Error('Invalid date');
  return d;
};
function parseNewArgs(argv) {
  return processArgs(
    argv,
    {
      '--date': dateArg,
      '--movie-id': [Number],
      '--yes': Boolean,
      '--single': Boolean,
      '--no-im': Boolean,
      '-d': '--date',
      '-i': '--movie-id',
      '-y': '--yes',
      '-s': '--single',
    },
  );
}
function parseLinkArgs(argv) {
  return processArgs(
    argv,
    {
      '--idx': Number,
      '--link': String,
      '--yes': Boolean,
      '--single': Boolean,
      '--no-im': Boolean,
      '-i': '--idx',
      '-l': '--link',
      '-y': '--yes',
      '-s': '--single',
    },
  );
}
function parseCountdownArgs(argv) {
  return processArgs(
    argv,
    {
      '--idx': Number,
      '--yes': Boolean,
      '--single': Boolean,
      '--no-im': Boolean,
      '-i': '--idx',
      '-y': '--yes',
      '-s': '--single',
    },
  );
}


async function waitYes(args) {
  if (args.yes) return true;
  let handler;
  console.log('Continue? (y/n) ');
  const yes = await new Promise(resolve => {
    handler = () => {
      while(process.stdin.readable) {
        const chBuf = process.stdin.read(1);
        if (!chBuf) return;
        const ch = chBuf.toString();
        if (ch === 'y') { resolve(true); return }
        if (ch === 'n') { resolve(false); return }
        console.error('Unknown response:', JSON.stringify(ch));
        console.log('Continue? (y/n) ');
      }
    };
    process.stdin.on('readable', handler);
  });
  process.stdin.off('readable', handler);
  return yes;
}

async function cmdNew(args) {
  const moviesLen = args.single ? 1 : 3;
  if (!(args.movieId && args.movieId.length === moviesLen))
    throw new Error(`Must give ${moviesLen} movie IDs`);
  if (!args.date)
    throw new Error('Must give date');
  const date = applyStartTime(args.date, args.single);

  const resps = await Promise.all(args.movieId.map(id => axios.get(
    `https://apis.justwatch.com/content/titles/movie/${id}/locale/en_AU`,
  )))

  const prefix = `${date.format('YYYY-MM-DD')}-netflix-party-`;
  const suffix = '.md';

  const movies = [];
  const m1start = date.clone();
  const m1end = m1start.clone().add(resps[0].data.runtime, 'minutes');
  movies.push({ data: resps[0].data, start: m1start, end: m1end });

  if (!args.single) {
    // const m2start = m1end.clone().add(30, 'minutes');
    // m2start.add(roundUp(m2start.get('minutes'), 15) - m2start.get('minutes'), 'minutes');
    const m2start = m1start.clone().add(2, 'hours');
    const m2end = m2start.clone().add(resps[1].data.runtime, 'minutes');
    movies.push({ data: resps[1].data, start: m2start, end: m2end });

    // const m3start = m2end.clone().add(5, 'minutes');
    // m3start.add(roundUp(m3start.get('minutes'), 5) - m3start.get('minutes'), 'minutes');
    const m3start = m2start.clone().add(2, 'hours');
    const m3end = m3start.clone().add(resps[2].data.runtime, 'minutes');
    movies.push({ data: resps[2].data, start: m3start, end: m3end });
  }

  console.log(movies[0].start.format('dddd Do MMMM'));
  for (const { data, start, end } of movies) {
    console.log(' ', data.title);
    console.log('   ', start.format('HH:mm'), ' - ', end.format('HH:mm'));
    console.log();
  }

  if (!await waitYes(args)) return;

  for (const [idx, m] of movies.entries()) {
    const filename = `${prefix}${slug(m.data.title)}${suffix}`;
    const fullPath = path.join(contentPath, filename);
    await fs.writeFile(fullPath, event({
      title: `Netflix Party - ${m.data.title}`,
      movieId: m.data.id,
      synopsisText: m.data.short_description,
      start: m.start,
      end: m.end,
      single: args.single,
      idx,
    }));
  }

  if (!(args.noIm || args.yes)) {
    console.log('Waiting for site update');
    if (!await waitYes(args)) return;
  }

  const msgBase = `
    **Next Netflix Party: ${movies[0].start.format('dddd Do MMMM')}**
    _${movies[0].start.format('hh:mma')} - ${movies[0].end.format('hh:mma')}_ ${movies[0].data.title}
  `;
  const msg = args.single
        ? msgBase
        : `${msgBase.trim()}
          _${movies[1].start.format('hh:mma')} - ${movies[1].end.format('hh:mma')}_ ${movies[1].data.title}
          _${movies[2].start.format('hh:mma')} - ${movies[2].end.format('hh:mma')}_ ${movies[2].data.title}
          `;

  await sendDiscord(
    msg,
    { im: !args.noIm },
  );
}
function validateIdx(idx, single = false) {
  if (!idx)
    throw new Error('Must give idx');
  if (idx < 1)
    throw new Error('Idx must be >= 1');
  const maxIdx = single ? 1 : 3;
  if (idx > maxIdx)
    throw new Error(`Idx must be <= ${maxIdx}`);
}
async function cmdLink(args) {
  validateIdx(args.idx, args.single);
  if (!args.link)
    throw new Error('Must give link');
  const date = moment(); //nextDate(args.single);
  const files = await loadFiles(date, args.single);
  const file = files[args.idx - 1];
  console.log('Updating', file.frontmatter.title);

  if (!await waitYes(args)) return;

  await fs.writeFile(
    file.history[0],
    event({
      ...file.frontmatter,
      idx: args.idx - 1,
      partyLink: args.link,
      single: args.single,
    }),
  );
  const filename = path.basename(file.history[0])
  const slug = filename.substr(0, filename.length - 3);
  await sendDiscord(
    `
    **${titleToMovieName(file.frontmatter.title)}** at ${file.frontmatter.start.format('hh:mma')}
    _Party Link_ ${args.link}
    _Website Link_ https://www.vicpah.org.au/events/${slug}/
    `,
    { im: !args.noIm },
  );
}
function countdownInterval(start, sec, noIm) {
  const until = start.clone().subtract(sec, 'second').diff(moment());
  if (until < 0)
    return;
  const strtime = sec > 60
        ? moment.duration(sec, 'seconds').humanize()
        : `${sec} seconds`;
  setTimeout(() => {
    sendDiscord(
      sec === 0
        ? "Let's go!"
        : `${_.sample(emojis)} Movie starting in **${strtime}**`,
      { im: !noIm, cliPrefix: false },
    );
  }, until);
}
async function cmdCountdown(args) {
  validateIdx(args.idx, args.single);
  const date = moment(); //nextDate(args.single);
  const files = await loadFiles(date, args.single);
  const file = files[args.idx - 1];
  console.log('Counting down for', file.frontmatter.title);

  if (!await waitYes(args)) return;

  const c = (...args) => countdownInterval(...args, args.noIm);
  const { start } = file.frontmatter;
  c(start, 0);
  c(start, 1);
  c(start, 2);
  c(start, 3);
  c(start, 10);
  c(start, 30);
  c(start, 60);
  c(start, 60 * 2);
  c(start, 60 * 5);
  c(start, 60 * 10);
}

const cmdDesc = {
  new: 'create a new netflix party (3x events)',
  link: 'link update the next netflix party event with a link, and send Discord update',
  countdown: 'start sending countdowns to Discord'
}
const cmdUsage = {
  help: () => `${Object.keys(cmdDesc).join('|')}

  new       : ${cmdDesc.new}
  link      : ${cmdDesc.link}
  countdown : ${cmdDesc.countdown}
  help      : this message
`,

  new: () => `--movie-id|-i ID [--yes|-y] [--single|-s] [--no-im]

OPTIONS:
  -i, --movie-id
          JustWatch movie ID to add to the movie night.

          This arg must be given exactly 3 times.

          Site link:
          https://www.justwatch.com/au/provider/netflix/movies

  -y, --yes
          Create without asking for confirmation.

  -s, --single
          Single movie mode.

  -d, --date
          Date to create the event on.

      --no-im
          Don't send IM messages
`,
  link: () => `--idx|-i MOVIE_INDEX --link|-l PARTY_URL [--yes|-y] [--single|-s] [--no-im]

OPTIONS:
  -i, --idx
          Index (starting at 1) of the movie in the party to provide the link
          for.

  -l, --link
          URL of the Netflix Party to join.

  -y, --yes
          Update without asking for confirmation.

  -s, --single
          Single movie mode.

      --no-im
          Don't send IM messages
`,
  countdown: () => `--idx|-i MOVIE_INDEX [--yes|-y] [--single|--s] [--no-im]

OPTIONS:
  -i, --idx
          Index (starting at 1) of the movie in the party to provide the link
          for.

  -y, --yes
          Send without asking for confirmation.

  -s, --single
          Single movie mode.

      --no-im
          Don't send IM messages
`,
};
const usage = ({ cmd = 'help', out = console.error, errorMsg = null } = {}) => {
  out(`usage: netflix_party.sh ${cmd === 'help' ? '' : cmd + ' '}${cmdUsage[cmd]()}`);
  if (errorMsg) {
    out('ERROR:', errorMsg);
  }
};


async function main() {
  const cmd = process.argv[2];
  const remain = process.argv.slice(3);

  if (
    cmd
      && process.argv.indexOf('--help') !== -1
      && cmdUsage[cmd]
  ) {
    usage({ cmd, out: console.log });
    return null;
  }

  if (cmd === 'new') {
    return cmdNew(parseNewArgs(remain));
  } else if (cmd === 'link') {
    return cmdLink(parseLinkArgs(remain));
  } else if (cmd === 'countdown') {
    return cmdCountdown(parseCountdownArgs(remain));
  } else {
    usage({ errorMsg: 'Unknown command' });
  }
}


module.exports = {
  main,
};
