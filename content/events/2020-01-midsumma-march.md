---
title: Midsumma pride march
timezone: australia/melbourne
start: 2020-02-02T10:00:00
end: 2020-02-02T13:30:00
link: https://www.midsumma.org.au/info/midsumma-pride-march/
facebook: https://www.facebook.com/midsumma
eventGroups: [vicpah, victoria, midsumma, major, free]
dressCode: |-
  Please wear either puppy gear or something that shows off your puppy pride,
  and something that you can stay cool in.
adr:
  street_address: Fitzroy St
  suburb: St Kilda
  state: VIC
---
Vicpah will be marching in the Midsumma Pride March once again this year. We will be marching
alongside dozens of other LGBTQIA+ groups, and we'd love it if you could join us in celebrating
our pride as pups and handlers.

Bottled water and sunscreen will be provided.
