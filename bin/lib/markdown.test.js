const fse = require('fs-extra')
const os = require('os')
const path = require('path')

const { MarkdownFile } = require('./markdown')


function tmpPath() {
  return path.join(
    os.tmpdir(),
    '' + Math.floor(Math.random() * 1000000),
  )
}


const testPath = tmpPath()
afterAll(() => {
  fse.removeSync(testPath)
})

async function setupFile(lines) {
  const filename = path.join(testPath, 'test.md')
  await fse.emptyDir(testPath)
  await fse.outputFile(filename, lines.join('\n'))
  return filename
}

describe('MarkdownFile', () => {
  describe('getFrontmatter()', () => {
    test('from real file', async () => {
      const filename = await setupFile([
        '---',
        'testkey: v',
        'testarrinline: [v1, v2]',
        'testarrrows:',
        '  - v1',
        '  - v2',
        'testobjinline: {k1: v1, k2: v2}',
        'testobjrows:',
        '  k1: v1',
        '  k2: v2',
        '---',
        'some content'
      ])
      const md = new MarkdownFile(filename)
      expect(await md.getFrontmatter()).toEqual({
        testkey: 'v',
        testarrinline: ['v1', 'v2'],
        testarrrows: ['v1', 'v2'],
        testobjinline: {k1: 'v1', k2: 'v2'},
        testobjrows: {k1: 'v1', k2: 'v2'},
      })
    })
  })
  describe('getContentText()', () => {
    test('from real file', async () => {
      const filename = await setupFile([
        '---',
        'testkey: v',
        '---',
        '# some content',
        'boop',
      ])
      const md = new MarkdownFile(filename)
      expect(await md.getContentText()).toEqual([
        '# some content',
        '',
        'boop',
        '',
      ].join('\n'))
    })
  })
  describe('getContentHtml()', () => {
    test('from real file', async () => {
      const filename = await setupFile([
        '---',
        'testkey: v',
        '---',
        '# some content',
        'boop',
      ])
      const md = new MarkdownFile(filename)
      expect(await md.getContentHtml()).toEqual([
        '<h1>some content</h1>',
        '<p>boop</p>',
        '',
      ].join('\n'))
    })
  })
})
