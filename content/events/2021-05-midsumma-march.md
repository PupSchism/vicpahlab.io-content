---
title: Midsumma pride march
timezone: australia/melbourne
start: 2021-05-23T09:15:00
end: 2021-05-23T16:00:00
link: https://www.midsumma.org.au/info/midsumma-pride-march/
facebook: https://www.facebook.com/midsumma
eventGroups: [vicpah, victoria, midsumma, major, free]
dressCode: |-
  Please wear either puppy gear or something that shows off your puppy pride.
adr:
  street_address: Fitzroy St
  suburb: St Kilda
  state: VIC
tryBookingEid: 758292
---
Vicpah will be marching in the Midsumma Pride March once again this year. We will be marching
alongside dozens of other LGBTQIA+ groups, and we'd love it if you could join us in celebrating
our pride as pups and handlers.

This year, it's important that you register by booking a ticket.

## Not Marching? Planning on watching the Midsumma Pride March this year on the 23rd of May?
Don't forget, you cannot simply stand by the road and watch the march this year due to
Covidsafe restrictions.

There are only three ways to spectate the Midsumma Pride Parade.

Book into a venue along Fitzroy St, find a venue near you that will be streaming the event,
or create your own house party and watch from home with friends.

Midsumma Pride March will be streamed live from the Midsumma Facebook on the 23rd of May from
approx 10:45am and conclude approx 2pm (times to be updated as plans are locked in).

## More informmation
Check out the information sheet for the march! We will be updating this document closer to the
date with all relevant info.

[https://bit.ly/VicPAHPrideMarch21](https://bit.ly/VicPAHPrideMarch21)
